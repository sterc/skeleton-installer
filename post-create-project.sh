# Copy over the skeleton-base files to the project root
cp -R ./vendor/sterc/skeleton-base/webroot/* .
cp -R ./vendor/sterc/skeleton-base/.docker ../.docker
cp -R ./vendor/sterc/skeleton-base/src ../src
cp ./vendor/sterc/skeleton-base/.deployignore ../
cp ./vendor/sterc/skeleton-base/.gitignore ../
cp ./vendor/sterc/skeleton-base/package.json ../
cp ./vendor/sterc/skeleton-base/webpack.common.js ../
cp ./vendor/sterc/skeleton-base/webpack.dev.js ../
cp ./vendor/sterc/skeleton-base/webpack.prod.js ../

# Copy the coding-standards files to the project root
cp ./vendor/sterc/coding-standards/coding-standards/.eslintrc.yml ../
cp ./vendor/sterc/coding-standards/coding-standards/.phpcs.xml ../
cp ./vendor/sterc/coding-standards/coding-standards/.phpmd.xml ../
cp ./vendor/sterc/coding-standards/coding-standards/.phplint.yml ../
cp ./vendor/sterc/coding-standards/coding-standards/.stylelintrc.json ../
cp ./vendor/sterc/coding-standards/coding-standards/bitbucket-pipelines.yml ../

# Run the npm scripts
cd ../ && npm install && npm run dev
