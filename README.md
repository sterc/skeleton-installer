# Sterc - Skeleton installer
This is a public installer for Sterc skeleton, using the sterc/skeleton-base package (https://bitbucket.org/sterc/skeleton-base/).
The composer package for this is here: https://packagist.org/packages/sterc/skeleton
This will install the skeleton-base package in your project webroot folder using composer, and also copy all the needed files from the vendor folder to the root folder.

## Installation - new project
1. Create a new project directory on your local environment, for example `www/my-project.nl`
2. Run command `composer create-project sterc/skeleton webroot`  
This will install the composer package for sterc/skeleton inside `webroot` folder.
Also the `post-create-project.sh` script will be executed, which will copy the needed files from the vendor folder to the root folder.
4. If you only need the frontend files, you're done! The source js and scss files are located in the `src` folder, and the html files are in `webroot/html` folder.
For compiling the js and scss files, run `npm install` and `npm run dev` from the root folder.
5. For installing MODX, the MODX skeleton and modules run the following command:
`php webroot/vendor/bin/skeleton skeleton:install`
This script will then guide you through the installation process.
